<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDnAttributes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dn_attributes', function (Blueprint $table) {
            $table->integer('level');
            $table->integer('armor_divisor');
            $table->integer('element_armor_divisor');
            $table->integer('element_attack_divisor');
            $table->integer('critical_chance_divisor');
            $table->integer('critical_damage_divisor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dn_attributes');
    }
}
