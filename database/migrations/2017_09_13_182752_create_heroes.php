<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeroes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('heroes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('role1');
            $table->string('role2');
            $table->integer('ad');
            $table->integer('ap');
            $table->integer('hp');
            $table->integer('mp');
            $table->integer('armor');
            $table->integer('magic_resist');
            $table->integer('attack_speed');
            $table->integer('speed');
            $table->integer('health_regen');
            $table->integer('magic_regen');
            $table->integer('attack_range');
            $table->integer('max_ad');
            $table->integer('max_ap');
            $table->integer('max_hp');
            $table->integer('max_mp');
            $table->integer('max_armor');
            $table->integer('max_magic_resist');
            $table->integer('max_attack_speed');
            $table->integer('max_speed');
            $table->integer('max_health_regen');
            $table->integer('max_magic_regen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('heroes');
    }
}
