<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DnAttribute extends Model
{
    public static function getDnAttributes($level, $attack, $critical, $ele_attack, $ele_armor, $armor, $hp) {
        $divisors = self::getDivisors($level);
        return [
            "ele_attack_boost_rate" => self::getEleAttackBoostRate($ele_attack, $divisors["element_attack_divisor"]),
            "critical_chance" => self::getCriticalChange($critical, $divisors["critical_chance_divisor"]),
            "armor_reduction_rate" => self::getArmorDamageReductionRate($armor, $divisors["armor_divisor"]),
            "ele_armor_reduction_rate" => self::getEleArmorDamageReductionRate($ele_armor, $divisors["element_armor_divisor"]),
            "advices" => self::getAdvices($attack, $critical, $armor, $hp, $divisors["critical_chance_divisor"], $divisors["armor_divisor"])
        ];
    }

    public static function getAdvices($attack, $critical, $armor, $hp, $critical_divisor, $armor_divisor) {
        $advices = [];
        $data1 = self::getOptimalAttackCritical($attack, $critical, $critical_divisor);
        if ($data1["optimal_attack"] > $attack) {
            $advices[] = "提升攻击到" . $data1["optimal_attack"] . "，以达到攻击和致命的最优配比";
        }
        if ($data1["optimal_critical"] > $critical) {
            $advices[] = "提升致命一击到" . $data1["optimal_critical"] . "，以达到攻击和致命的最优配比";
        }
        $data2 = self::getOptimalArmorHp($armor, $hp, $armor_divisor);
        if ($data2["optimal_armor"] > $armor) {
            if ($data2["optimal_armor"] < $data2["armor_threshold"]) {
                $advices[] = "提升物防到" . $data2["optimal_armor"] . "，以达到物防和生命的最优配比";
            } else {
                $advices[] = "提升物防到该等级物防上限值" . $data2["optimal_armor"] . "，然后才考虑提升生命";
            }

        }
        if ($data2["optimal_hp"] > $hp) {
            $advices[] = "提升生命值到" . $data2["optimal_hp"] . "，以达到物防和生命的最优配比";
        }
        if ($data2["armor_threshold"] < $armor) {
            $advices[] = "物防超过当前等级上限" . $data2["armor_threshold"] . "，不要再提升物防了";
        }
        return $advices;
    }

    public static function getOptimalAttackCritical($attack, $critical, $divisor) {
        $tmp = $critical + $divisor;
        $optimal_attack = (1 + $critical/$tmp) / ((-2 * $critical) / ($tmp * $tmp) + 2/$tmp);
        $optimal_critical = 0.25 * (-3 * $divisor + sqrt($divisor) * sqrt(16 * $attack + $divisor));
        return [
            "optimal_attack" => round($optimal_attack),
            "optimal_critical" => round($optimal_critical)
        ];
    }

    public static function getOptimalArmorHp($armor, $hp, $divisor) {
        $armor_threshold = 17/3 * $divisor;
        $real_armor = min($armor_threshold, $armor);
        $optimal_hp = 20 * ($divisor + $real_armor);
        $optimal_armor = min($armor_threshold, -$divisor + $hp/20);
        return [
            "optimal_hp" => round($optimal_hp),
            "optimal_armor" => round($optimal_armor),
            "armor_threshold" => round($armor_threshold)
        ];
    }

    public static function getDivisors($level) {
        $attr_upper = DnAttribute::where('level', ">=", $level)->orderBy("level")->first();
        $attr_lower = DnAttribute::where('level', "<=", $level)->orderBy("level", "desc")->first();
        return [
            "armor_divisor" => self::calcDivisor($attr_upper, $attr_lower, $level, "armor_divisor"),
            "element_armor_divisor" => self::calcDivisor($attr_upper, $attr_lower, $level, "element_armor_divisor"),
            "element_attack_divisor" => self::calcDivisor($attr_upper, $attr_lower, $level, "element_attack_divisor"),
            "critical_chance_divisor" => self::calcDivisor($attr_upper, $attr_lower, $level, "critical_chance_divisor")
        ];
    }

    public static function calcDivisor($upper, $lower, $level, $name) {
        if ($upper->level == $lower->level) {
            return $lower->{$name};
        }
        return intval($lower->{$name} + ($upper->{$name} - $lower->{$name}) / ($upper->level - $lower->level) * ($level - $lower->level));
    }

    public static function getEleAttackBoostRate($n, $divisor) {
        return $n / ($n + $divisor);
    }

    public static function getCriticalChange($n, $divisor) {
        return $n / ($n + $divisor);
    }

    public static function getEleArmorDamageReductionRate($n, $divisor) {
        return min(0.85, $n / ($n + $divisor));
    }

    public static function getArmorDamageReductionRate($n, $divisor) {
        return min(0.85, $n / ($n + $divisor));
    }

    public static function getRealHitPoint($hp, $armor, $armor_divisor) {
        $rate = self::getArmorDamageReductionRate($armor, $armor_divisor);
        return $hp / (1 - $rate);
    }

    public static function getAttackMultiplier($attack, $ele_attack, $ele_attack_divisor, $critical, $critical_divisor) {
        $ch = self::getCriticalChange($critical, $critical_divisor);
        $ebr = self::getEleAttackBoostRate($ele_attack, $ele_attack_divisor);
        return $attack * (1 + $ch) * $ebr;
    }

}
