<?php

namespace App\Http\Controllers;

use App\DnAttribute;
use App\Sdown;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use NumberFormatter;

class DnController extends Controller
{
    public function calculator() {
        return view('dragon_nest.calculator');
    }

    public function faq() {
        $pd = new Sdown();
        $text = Storage::disk('local')->get('article/faq.md');
        $html = $pd->text($text);
        $title = "蓝猫淘气三千问";
        return view('dragon_nest.article', [
            "title" => $title,
            "html" => $html
        ]);
    }

    public function attribute(Request $request) {
        $data = DnAttribute::getDnAttributes($request->level, $request->attack, $request->critical, $request->ele_attack, $request->ele_armor, $request->armor, $request->hp);
        $formatter = new NumberFormatter('en_US', NumberFormatter::PERCENT);
        $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
        $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, 2);
        $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, 2);
        foreach ($data as $k => $datum) {
            if ($k == "advices") {
                continue;
            }
            $data[$k . "_p"] = $formatter->format($datum);
        }
        return json_encode($data);
    }
}
