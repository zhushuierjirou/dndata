@extends('layout.main')

@section("title","云南配装老司机")
@section("style")
    <style>
        .none {
            display: none;
        }

        .progress-bar-text {
            text-align: right;
            padding-left: 0;
            padding-right: 30px;
        }

        .progress-bar-min {
            min-width: 6em;
        }
        .panel-title {
            font-weight: bold;
        }
    </style>
@endsection
@section("content")
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">云南配装老司机(支持70~100级查询)</h3>
                </div>
                <div class="panel-body">
                    <form class="form-horizontal" id="queryForm">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="levelInput">等级</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="levelInput" min="70" max="100" value="90"
                                       name="level" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="attackInput">攻击</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="attackInput" value="100000" name="attack"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="criticalInput">致命</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="criticalInput" value="50000"
                                       name="critical" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="eleAttackInput">属性攻</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="eleAttackInput" value="84"
                                       name="ele_attack" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="eleDefenceInput">属性防</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="eleDefenceInput" value="84"
                                       name="ele_armor" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="defenceInput">物理防御</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="defenceInput" value="50000" name="armor"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-lg-1 control-label" for="hpInput">生命值</label>
                            <div class="col-sm-10 col-lg-11">
                                <input type="number" class="form-control" id="hpInput" value="2000000" name="hp"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10 col-lg-offset-1 col-lg-11">
                                <button class="btn btn-default">开车</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="panel panel-default none" id="result-panel">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">属性分析</h3>
                </div>
                <div class="panel-body">
                    <div>
                        <label class="col-sm-2 col-lg-1 progress-bar-text">属攻提升</label>
                        <div class="progress">
                            <div id="ele_attack_boost_rate" class="col-sm-10 col-lg-11 progress-bar progress-bar-min"
                                 role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 20%;">
                                20%
                            </div>
                        </div>
                    </div>
                    <div>

                        <label class="col-sm-2 col-lg-1 progress-bar-text">属防减伤</label>
                        <div class="progress">
                            <div id="ele_armor_reduction_rate" class="col-sm-10 col-lg-11 progress-bar progress-bar-min"
                                 role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 20%;">
                                20%
                            </div>
                        </div>
                    </div>
                    <div>
                        <label class="col-sm-2 col-lg-1 progress-bar-text">物防减伤</label>
                        <div class="progress">
                            <div id="armor_reduction_rate" class="col-sm-10 col-lg-11 progress-bar progress-bar-min"
                                 role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 20%;">
                                20%
                            </div>
                        </div>
                    </div>
                    <div>
                        <label class="col-sm-2 col-lg-1 progress-bar-text">致命几率</label>
                        <div class="progress">
                            <div id="critical_chance" class="col-sm-10 col-lg-11 progress-bar progress-bar-min"
                                 role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                 style="width: 20%;">
                                20%
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default none" id="advice-panel">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">配装建议</h3>
                </div>
                <ul class="list-group" id="advice-list">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Cras justo odio</li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section("js")
    <script>
        $("#queryForm").ajaxForm(
            {
                url: '/dn/attribute',
                type: 'post',
                success: function (res) {
                    res = JSON.parse(res);
                    $('#result-panel').removeClass("none")
                    if (res.advices) {
                        $('#advice-panel').removeClass("none")
                    } else {
                        $('#advice-panel').addClass("none")
                    }
                    $('#ele_attack_boost_rate').text(res.ele_attack_boost_rate_p)
                    $('#ele_attack_boost_rate').attr('aria-valuenow', Math.round(Number(res.ele_attack_boost_rate * 100)))
                    $('#ele_attack_boost_rate').css('width', res.ele_attack_boost_rate_p)

                    $('#ele_armor_reduction_rate').text(res.ele_armor_reduction_rate_p)
                    $('#ele_armor_reduction_rate').attr('aria-valuenow', Math.round(Number(res.ele_armor_reduction_rate * 100)))
                    $('#ele_armor_reduction_rate').css('width', res.ele_armor_reduction_rate_p)

                    $('#armor_reduction_rate').text(res.armor_reduction_rate_p)
                    $('#armor_reduction_rate').attr('aria-valuenow', Math.round(Number(res.armor_reduction_rate * 100)))
                    $('#armor_reduction_rate').css('width', res.armor_reduction_rate_p)

                    $('#critical_chance').text(res.critical_chance_p)
                    $('#critical_chance').attr('aria-valuenow', Math.round(Number(res.critical_chance * 100)))
                    $('#critical_chance').css('width', res.critical_chance_p)
                    $('#advice-list').empty();
                    for (i=0; i<res.advices.length; ++i) {
                        $('#advice-list').append('<li class="list-group-item"><label>' + (i+1) + ". " + res.advices[i] + '</label></li>')
                    }
                    setTimeout(function () {
                        document.getElementById("result-panel").scrollIntoView();
                    }, 300)
                }
            }
        )
    </script>
@endsection