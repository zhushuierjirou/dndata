@extends('layout.main')

@section("title")
    {{ $title }}
@endsection
@section("style")
    <style>
        img {
            width: 100%;
            max-width: 500px;
        }
    </style>
@endsection
@section("content")
    <div class="container">
        <div class="row">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">{{ $title }}</h3>
                </div>
                <div class="panel-body">
                    {!! $html !!}
                </div>
            </div>
        </div>
    </div>
@endsection

