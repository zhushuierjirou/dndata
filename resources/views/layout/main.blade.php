@extends('layout.basic')

@section('body')
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/" style="font-family: 'Raleway', sans-serif;">WYG</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    @foreach(\App\NavList::nav_list as $nav_url => $nav_name)
                        <li class="{{$nav_url == app('request')->path()?"active" : " "}}"><a href="{{$nav_url}}">{{$nav_name}}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>
    @yield('content')
    <div class="container text-center">
        <div class="row">
            <hr>
            <p>
                <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=934afd47c26428e91d7f50cf3077a328a696175570b4ed56c203355bca528de7">
                    QQ群:248373027
                </a>
            </p>
        </div>
    </div>
@endsection
